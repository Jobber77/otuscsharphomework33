﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lib
{
    public class RabbitMqOptions
    {
        public string Host { get; set; }
        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Queue { get; set; }
    }
}
