﻿using Lib;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = GetConfig();
            var factory = new ConnectionFactory()
            {
                HostName = options.Host,
                VirtualHost = options.VirtualHost,
                UserName = options.UserName,
                Password = options.Password
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: options.Queue,
                                    durable: true,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            Console.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body);
                var user = JsonSerializer.Deserialize<User>(message);
                if (user == null || string.IsNullOrEmpty(user.Email))
                    return;
                Console.WriteLine(" [x] Received user {0}", message);
                Console.WriteLine(" [x] Done");
                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
            channel.BasicConsume(queue: options.Queue,
                                    autoAck: false,
                                    consumer: consumer);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        private static RabbitMqOptions GetConfig() => new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build()
               .GetSection("RabbitMqOptions")
               .Get<RabbitMqOptions>();
    }
}
