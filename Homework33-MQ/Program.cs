﻿using AutoFixture;
using Lib;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading;

namespace Homework33_MQ
{
    class Program
    {
        private static string _queue;
        static void Main(string[] args)
        {
            var options = GetConfig();
            var factory = new ConnectionFactory()
            {
                HostName = options.Host,
                VirtualHost = options.VirtualHost,
                UserName = options.UserName,
                Password = options.Password
            };
            _queue = options.Queue;
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: options.Queue, durable: true, exclusive: false, autoDelete: false, arguments: null);
            using var timer = new Timer(state => SendUser(state), 
                                 state: channel, 
                                 dueTime: TimeSpan.FromMinutes(0), 
                                 period: TimeSpan.FromMinutes(1));

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        private static RabbitMqOptions GetConfig() => new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build()
                .GetSection("RabbitMqOptions")
                .Get<RabbitMqOptions>();

        private static void SendUser(object state)
        {
            IModel channel = state as IModel;
            if (channel == null)
                return;
            var user = new Fixture().Create<User>();
            var message = JsonSerializer.Serialize(user);
            var body = Encoding.UTF8.GetBytes(message);

            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;
            channel.BasicPublish(exchange: "",
                                 routingKey: _queue,
                                 basicProperties: properties,
                                 body: body);
            Console.WriteLine(" [x] Sent user: {0}", message);
        }

    }
}
